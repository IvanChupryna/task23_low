﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_Low.Controllers
{
    public class QuestionnaireController : Controller
    {
        private List<Dictionary<string, string>> _questionnaries = new List<Dictionary<string, string>>();
        // GET: Questionnaire
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Result()
        {
            return View();
        }

        [HttpPost]
        [ActionName("Result")]
        public ActionResult ResultPost()
        {
            return View();
        }
    }
}