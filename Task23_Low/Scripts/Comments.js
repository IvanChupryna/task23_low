﻿var comments = [];
var currentOutputBlock = document.getElementById("output_comments");
currentOutputBlock.onload = function () {
    comments.forEach(comment => {
        var username = document.createElement("p");
        username.innerHTML = comment["username"];
        currentOutputBlock.appendChild(username);

        var date = document.createElement("p");
        date.innerHTML = comment["date"];
        currentOutputBlock.appendChild(date);

        var content = document.createElement("p");
        content.innerHTML = comment["content"];
        currentOutputBlock.appendChild(content);
    })
}

var submitButton = document.getElementById("submit_button");
submitButton.onclick = function () {
    var userName = document.getElementById("username");
    var currentDate = Date.now();
    var Content = document.getElementById("content");
    var comment = {
        username: userName,
        date: currentDate,
        content: Content
    };
    comment.push(comment);
}